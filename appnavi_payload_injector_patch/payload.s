; Dont give me a hard time this was the first ASM thing Ive built in my entire life

BITS 64 
section .data
        ; strings

    ; You might wanna be careful with the part below. Basically if you have the navi_extended
    ; on both partitions, it will be executed twice. And because ASM is fking fast, you might
    ; not even have time to set some "file flags" to signal to the other process that you are 
    ; already running. So just heads up, you might have race conditions. 
    ; the best solution is to remember to remove one of the navi_extended 
    ; or just copy it to one place always and only execute that!. 
    
    program_name_navi    db "/navi/Bin/navi_extended", 0    
    program_name_navi2    db "/navi2/Bin/navi_extended", 0

    program_name    db "/usr/bin/ls", 0
    arg1            db "-l", 0    
    arg2            db "/", 0

    envp    dq 0

    msg dq "Hello World!",0xa, 0xd
    .len equ $-msg

    msg_child dq "We are child World!",0xa, 0xd
    .len equ $-msg_child

    msg_ret db "Returning to parent",0xa, 0xd
    .len equ $-msg_ret

    buffer: times 512 db 0


section .text
global main
main: 
    push rax         ; save all clobbered registers
    push rcx               
    push rdx
    push rsi
    push rdi
    push r11

    mov rax, 0x1    ; syscall number for write 
    mov rdi, 0x1    ; int fd 
    lea rsi, [rel msg]    ; const void* buf 
    lea rdx, [rel msg.len]   ; size_t count
    syscall

    ;fork
    xor rax,rax
    add rax,57
    syscall
    cmp eax, 0
    jz child 

parent:
    mov rax, 0x1    ; syscall number for write 
    mov rdi, 0x1    ; int fd 
    lea rsi, [rel msg_ret]    ; const void* buf 
    lea rdx, [rel msg_ret.len]   ; size_t count
    syscall

    pop r11          ; restore all registers
    pop rdi
    pop rsi
    pop rdx
    pop rcx
    pop rax

    ; jump to original entry point
    push <REPLACEWITHOEP>
    ret

child:
    mov rax, 0x1    ; syscall number for write 
    mov rdi, 0x1    ; int fd 
    lea rsi, [rel msg_child]    ; const void* buf 
    lea rdx, [rel msg_child.len]   ; size_t count
    syscall

    ; call print_link

    ;fork
    xor rax,rax
    add rax,57
    syscall
    cmp eax, 0
    jz execve 

    ;fork
    xor rax,rax
    add rax,57
    syscall
    cmp eax, 0
    jz navi 

    ;fork
    xor rax,rax
    add rax,57
    syscall
    cmp eax, 0
    jz navi2 

    call exit

; print_link:
;     push rax    
;     push rdi
;     push rdx
;     push rsi

;     mov rax, 89      ; sys_execve (59)
;     lea rdi, [rel proc_sef_exe] ; path
;     lea rsi, [rel buffer] ; buff
;     mov rdx, 256 ; buffer size
;     syscall


;     mov rax, 0x1    ; syscall number for write 
;     mov rdi, 0x1    ; int fd 
;     lea rsi, [rel buffer]    ; const void* buf 
;     lea rdx, 256   ; size_t count
;     syscall

;     ret

execve:
    push rax
    push rdi
    push rbx
    push rsi

    push 0
    lea rsi, [rel arg2]
    push rsi
    lea rsi, [rel arg1]
    push rsi
    lea rsi, [rel program_name]
    push rsi

    mov rax, 0x3b      ; sys_execve (59)
    lea rdi, [rel program_name] ; filanem
    mov rsi, rsp
    mov rdx, [rel envp] ; envp 0
    syscall
    call exit

navi:
    push rax
    push rdi
    push rbx
    push rsi

    push 0
    ; lea rsi, [rel arg2]
    ; push rsi
    ; lea rsi, [rel arg1]
    ; push rsi
    lea rsi, [rel program_name_navi]
    push rsi

    mov rax, 0x3b      ; sys_execve (59)
    lea rdi, [rel program_name_navi] ; filanem
    mov rsi, rsp
    mov rdx, [rel envp] ; envp 0
    syscall
    call exit

navi2:
    push rax
    push rdi
    push rbx
    push rsi

    push 0
    ; lea rsi, [rel arg2]
    ; push rsi
    ; lea rsi, [rel arg1]
    ; push rsi
    lea rsi, [rel program_name_navi2]
    push rsi

    mov rax, 0x3b      ; sys_execve (59)
    lea rdi, [rel program_name_navi2] ; filanem
    mov rsi, rsp
    mov rdx, [rel envp] ; envp 0
    syscall
    call exit
    
exit:
    mov rax, 60 ; sys_exit (60)
    mov rdi, 0  ; exit code
    syscall
