#!/bin/bash

USB_PATH=$(dirname $0)
cd $USB_PATH

mkdir -p $USB_PATH/STATUS_FLAGS/
if [ ! -f "$USB_PATH/STATUS_FLAGS/WIDEOPEN_SERVICE_FIRST_RUN_FLAG" ]; then  
  printf "\n----- FLAG [$USB_PATH/STATUS_FLAGS/WIDEOPEN_SERVICE_FIRST_RUN_FLAG] not found! Not the first run? -----\n" ;
  exit 1 ;
fi

echo "Hello from systemd! We are going to do the initial setup now. Several reboots expected!" >> $USB_PATH/INITIAL_SETUP_LOG.txt ;
sync ;
bash $USB_PATH/INITIAL_SETUP_SCRIPTS/extract_keys.sh || exit >> $USB_PATH/INITIAL_SETUP_KEY_EXTRACT_LOG.txt 2>&1 ;
sync ;
bash $USB_PATH/INITIAL_SETUP_SCRIPTS/restore_appnavi.sh || exit >> $USB_PATH/INITIAL_SETUP_APPNAVI_RESTORATION.txt 2>&1 ;
sync ;
echo "We have completed the initial setup!" >> $USB_PATH/INITIAL_SETUP_LOG.txt ;
sync ;
rm $USB_PATH/STATUS_FLAGS/WIDEOPEN_SERVICE_FIRST_RUN_FLAG
mv $USB_PATH/INITIAL_SETUP_SCRIPTS/completed_setup_sound_mp3 $USB_PATH/INITIAL_SETUP_SCRIPTS/completed_setup.mp3
sync ;
/sbin/reboot ;