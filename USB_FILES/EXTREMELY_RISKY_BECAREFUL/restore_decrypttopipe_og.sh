
#!/bin/bash

# THIS ONE IS REALY SERIOUS. FOR REAL. IF YOU BREAK THIS YOU CAN'T UPDATE ANYMORE.

USB_PATH=$(dirname $0)
cd $USB_PATH

printf "\n----- Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) -----\n" ;

# Check if we actually did the backup
if [ ! -f "/app/share/AppUpgrade/DecryptToPIPE_OG" ]; then
    printf "We couldn't find  [/app/share/AppUpgrade/DecryptToPIPE_OG]. Maybe it wasn't copied. We can't proceed. it's not safe." ;
    exit 1;
fi

# Flag to check if we have already performed the restore. This is to avoid any race condition and executing the same op twice for some reason.
if [ ! -f "$USB_PATH/DONE_RESTORING_DECRYPT_OG_FLAG" ]; then
    # Restore original DecryptToPIPE_OG (leaving the backup there anyways)
    printf "We couldn't find  [$USB_PATH/DONE_RESTORING_DECRYPT_OG_FLAG]. Which means we haven't restored yet; we will restore now." ;
    touch $USB_PATH/DONE_RESTORING_DECRYPT_OG_FLAG
    mount -o remount,rw / 2>&1 ;
    cp /app/share/AppUpgrade/DecryptToPIPE_OG /app/share/AppUpgrade/DecryptToPIPE
    mount -o remount,ro / 2>&1 ;
    sync ;
fi

printf "\n----- END Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) END -----\n" ;

sleep 10 ; 
/sbin/reboot ;
