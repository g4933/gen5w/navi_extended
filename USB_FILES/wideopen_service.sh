#!/bin/bash

USB_PATH=$(dirname $0)
cd $USB_PATH

mkdir -p $USB_PATH/STATUS_FLAGS/
if [ -f "$USB_PATH/STATUS_FLAGS/WIDEOPEN_SERVICE_FIRST_RUN_FLAG" ]; then
  printf "\n----- [$USB_PATH/STATUS_FLAGS/WIDEOPEN_SERVICE_FIRST_RUN_FLAG] found! We will do/continue the initial setup!-----\n" >> $USB_PATH/wideopen_service_out.txt 2>&1 ;
  bash $USB_PATH/wideopen_service_first_run.sh >> $USB_PATH/wideopen_service_out.txt 2>&1 ;
  sync ;
fi

echo "Hello from systemd!" >> $USB_PATH/wideopen_service_out.txt 2>&1 ;
sleep 10 ;