#!/bin/bash

USB_PATH=$(dirname $0)
cd $USB_PATH

mount -o remount rw /
cp ./wideopen.service /etc/systemd/system/wideopen.service
systemctl daemon-reload
systemctl start wideopen
systemctl enable wideopen
mount -o remount ro /
mkdir -p ${USB_PATH}/STATUS_FLAGS/ 
touch ${USB_PATH}/STATUS_FLAGS/WIDEOPEN_SERVICE_FIRST_RUN_FLAG
mv "$0" $0_INSTALLED