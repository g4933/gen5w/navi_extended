# navi_extended

This project has the tools patch your Headunit to allow full execute access to the user. The method replaces the AppNavi application temporarily so that navi_extended can patch the system to autorun `main_loop` scripts from usb.


# How does this work?
* `navi_extended` is something I wrote that just finds your USB and executes a shell script on it. 

* In order for the headunit to execute our hack, we need to replace the official `AppNavi` application and use my `navi_extended` instead. (Rename `navi_extended` to => `AppNavi` and place it in `/Bin`) 

* Once we gain execution, we can **extract** the encryption keys and the official `AppNavi` in the unit. 

* With the now `official AppNavi` in our possession, we can now **patch** it so it will always call `navi_extended` when anybody calls `AppNavi`. Once it has called `navi_extended` it will go ahead and proceed with the regular `AppNavi` tasks, giving you your maps back.

# How to download? 
Go here https://gitlab.com/g4933/gen5w/navi_extended/-/releases and under Assets you will see "AppNavi" download that. 

# Steps to get it working

## WARNING
**Doing this will temporarily replace the official `AppNavi` and thus making your maps not work in the unit. If you finish all the steps you will get your maps + the hack working side by side. If at any point you give up, just reinstall your OS and that will get rid of all you've done.**
----

MAKE SURE YOU HAVE AN OFFICIAL UPDATE ON YOUR USB, AS WE USE IT TO EXTRACT THE NAVIGATION APP!

1. Head over to https://gitlab.com/g4933/gen5w/navi_extended/-/releases and under assets just download `USB Package (includes AppNavi)`, this is a ready to go USB folder structure with the needed files.
1. Extract the `.tar.gz` file locally
1. Copy the contents of the path `-/USB_FILES/` to an exFAT formatted USB drive
1. Then plug your USB into your Head Unit (HU) and do this stuff: https://www.youtube.com/watch?v=vU8nG18sFQ4 -- Or do the following

	1. *This method will only work if you are running an older, unpatched version of your nav system. If you see "This feature is currently unavailable" in your `config` tab you will need to downgrade your HU.*
	1. Start up HU normally
	1. Enter `Engineering Mode` (model dependent)
	1. Open the `Dynamics` page
	1. Open the `Navigation` page
	1. On left hand side select the `Config` sub-menu
	1. Select "Update AppNavi from USB"
	1. Select the left side button in the dialog "네" (yes)
	1. The next popup will say "USB Not Mounted", just click "확인" (Okay).

1. *There will be no indication the install is running except the maps and navigation screens will be unavailable*
1. Your device will restart several times! This is normal! Do not unplug the USB until the maps work again and if you go to media > usb device you can play rick roll's song.
1. After the process has finished your USB will have a few files copied to it that you will need for future steps. Make sure to save off the `DecryptToPIPE` and `.der` files from your thumb drive.

# Next steps
In order to perform official updates from Hyundai/Kia on the headunit, you'll need to patch all of your future update files as well as disable update file checking in the HU. Head over to [update-patcher](https://gitlab.com/g4933/gen5w/update-patcher) and [update_decryptor](https://gitlab.com/g4933/gen5w/update_decryptor) to get started.

# Useful Links
- Force Updating AppNavi in HU
https://youtu.be/vU8nG18sFQ4

- Using vol knob to enter in Engineering Menu
https://youtu.be/vhcPbpYW5Vk

# F.A.Q

**Can I update my HU after patching AppNavi? Will I lose my navi_extended?**

Yes, you can update. And yes, you will loose navi_extended doing so. This is assuming you are installing an official update. 

If you install an unencrypted update that you've modified, then you can embed the navi_extended on the install files yourself, that way the newly installed version will be pre-hacked. 

**My Config menu doesn't have "Update AppNavi from USB", just "This feature is currently unavailable"**

You will need to downgrade your HU, go over to [update_fetcher](https://gitlab.com/g4933/gen5w/update_fetcher) and use the tool to look for an older update for you HU. Copy all of the downgraded files to your usb for the HU but switch out the `.lge.upgrade.xml` file with a version from the latest update available for your vehicle's headunit.

Alternatively, you can also try to swap out all the `navi_eu` folder contents with an older (but not too old!) version, that will also re-enable the option to install the hack. (as the option depends on the AppNavi)

# Install Steps (legacy)

1. Replace AppNavi with our App (`navi_extended`)
1. On the shell script from your USB you will copy the AppNavi from both partitions `/navi` and `/navi2`, the official AppNavi will be on one of those folders (we also have it available because we can decrypt the update package)
1. You will rename `/app/share/AppUpgrade/DecryptToPipe` -> `/app/share/AppUpgrade/DecryptToPipe_OG`
1. You will create a bash script (first line should contain `#!/bin/bash`) and place it on `/app/share/AppUpgrade/DecryptToPipe` (essentially the old real path) that will first check if you have a specific file on your USB (THIS IS IMPORTANT FOR FAILSAFE) and depending on whether the file exists or not you will either call DecryptToPipe_OG and proceed with the regular update procedure, or call another arbitrary script of your choosing to piggyback on the Update Process to trigger your exploit BEFORE any update id handled by the headunit
